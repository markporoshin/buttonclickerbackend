package com.p0mami;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ButtonClickerApplication {

	private static final Logger logger = LoggerFactory.getLogger(ButtonClickerApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ButtonClickerApplication.class, args);
		logger.info("Started........");
	}

}
