CREATE TABLE users
(
    id integer NOT NULL ,
    token varchar(100) DEFAULT NULL,
    counter integer DEFAULT NULL,
    PRIMARY KEY (employeeId),
    UNIQUE (counter)
);