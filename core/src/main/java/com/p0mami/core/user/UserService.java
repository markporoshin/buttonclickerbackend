package com.p0mami.core.user;

import com.p0mami.restmodel.model.filter.UserFilter;
import com.p0mami.restmodel.model.post.UserPost;

public interface UserService {

    UserPost increment(UserFilter filter);

    UserPost findOrCreate(UserFilter filter);

}
