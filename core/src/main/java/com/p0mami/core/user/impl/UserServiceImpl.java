package com.p0mami.core.user.impl;

import com.p0mami.core.user.UserService;
import com.p0mami.domain.user.UserDomainService;
import com.p0mami.models.User;
import com.p0mami.restmodel.model.exceprion.ObjectNotFound;
import com.p0mami.restmodel.model.filter.UserFilter;
import com.p0mami.restmodel.model.post.UserPost;
import org.springframework.stereotype.Component;

@Component
public class UserServiceImpl implements UserService {

    private final UserDomainService userDomainService;

    public UserServiceImpl(UserDomainService userDomainService) {
        this.userDomainService = userDomainService;
    }


    @Override
    public UserPost increment(UserFilter filter) {
        if (filter == null || filter.getToken() == null) {
            throw new ObjectNotFound("token must be not null");
        }
        User user = userDomainService.incrementByToken(filter.getToken());
        if (user == null) {
            throw new ObjectNotFound("user not found");
        }
        UserPost userPost = new UserPost();
        userPost.setCounter(user.getCounter());
        return userPost;
    }

    @Override
    public UserPost findOrCreate(UserFilter filter) {
        if (filter == null || filter.getToken() == null) {
            throw new ObjectNotFound("token must be not null");
        }
        User user = userDomainService.getOrCreate(filter.getToken());
        UserPost userPost = new UserPost();
        userPost.setCounter(user.getCounter());
        return userPost;
    }
}
