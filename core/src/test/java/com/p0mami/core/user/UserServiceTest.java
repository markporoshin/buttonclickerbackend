package com.p0mami.core.user;

import com.p0mami.core.user.impl.UserServiceImpl;
import com.p0mami.domain.user.UserDomainService;
import com.p0mami.models.User;
import com.p0mami.restmodel.model.exceprion.ObjectNotFound;
import com.p0mami.restmodel.model.filter.UserFilter;
import com.p0mami.restmodel.model.post.UserPost;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.List;
import java.util.function.Function;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class UserServiceTest {


    private static final UserDomainService domainService = Mockito.mock(UserDomainService.class);
    private static final UserService userService = new UserServiceImpl(domainService);

    private static List<Arguments> throwsArguments() {
        return asList(
                Arguments.of(
                        null,
                        "token",
                        userOf(1L, "", 1L),
                        (Function<UserFilter, UserPost>) userService::increment
                ),
                Arguments.of(
                        filterOf(null),
                        "token",
                        userOf(1L, "", 1L),
                        (Function<UserFilter, UserPost>) userService::increment
                ),
                Arguments.of(
                        filterOf("token"),
                        "token",
                        null,
                        (Function<UserFilter, UserPost>) userService::increment
                ),
                Arguments.of(
                        filterOf(null),
                        "token",
                        null,
                        (Function<UserFilter, UserPost>) userService::findOrCreate
                ),
                Arguments.of(
                        null,
                        "token",
                        null,
                        (Function<UserFilter, UserPost>) userService::findOrCreate
                )
        );
    }

    @ParameterizedTest
    @MethodSource("throwsArguments")
    void testThrowsCases(
            UserFilter filter,
            String token,
            User expectedUser,
            Function<UserFilter, UserPost> function
    ) {
        when(domainService.findUserByToken(token)).thenReturn(expectedUser);

        assertThrows(ObjectNotFound.class, () -> {
            function.apply(filter);
        });
    }

    private static List<Arguments> positiveArguments() {
        return asList(
                Arguments.of(
                        filterOf("token"),
                        "token",
                        userOf(1L, "", 1L),
                        (Function<UserFilter, UserPost>) userService::findOrCreate,
                        userPostOf(1L)
                ),
                Arguments.of(
                        filterOf("token"),
                        "token",
                        userOf(1L, "", 2L),
                        (Function<UserFilter, UserPost>) userService::increment,
                        userPostOf(2L)
                )
        );
    }

    @ParameterizedTest
    @MethodSource("positiveArguments")
    void testPositiveCases(
            UserFilter filter,
            String token,
            User expectedUser,
            Function<UserFilter, UserPost> function,
            UserPost expectedResult
    ) {
        when(domainService.findUserByToken(token)).thenReturn(expectedUser);
        when(domainService.getOrCreate(token)).thenReturn(expectedUser);
        when(domainService.incrementByToken(token)).thenReturn(expectedUser);

        assertEquals(function.apply(filter).getCounter(), expectedResult.getCounter());
    }

    private static UserFilter filterOf(String token) {
        return new UserFilter(token);
    }
    private static User userOf(Long id, String token, Long counter) {
        return new User(id, token, counter);
    }
    private static UserPost userPostOf(Long counter) {
        return new UserPost(counter);
    }
}