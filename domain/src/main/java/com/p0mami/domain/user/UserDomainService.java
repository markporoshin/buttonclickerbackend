package com.p0mami.domain.user;

import com.p0mami.models.User;
import org.springframework.stereotype.Component;

@Component
public class UserDomainService {

    private final UserRepository userRepository;


    public UserDomainService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getOrCreate(String token) {
        User user = findUserByToken(token);
        if (user == null) {
            user = new User();
            user.setCounter(0L);
            user.setToken(token);
            userRepository.save(user);
        }
        return user;
    }

    public User incrementByToken(String token) {
        User user = findUserByToken(token);
        if (user == null) {
            return null;
        }
        user.setCounter(user.getCounter() + 1);
        return update(user);
    }

    public User update(User user) {
        return userRepository.save(user);
    }

    public User findUserByToken(String token) {
        return userRepository.findByByToken(token);
    }
}
