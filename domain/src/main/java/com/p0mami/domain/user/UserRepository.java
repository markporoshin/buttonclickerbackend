package com.p0mami.domain.user;

import com.p0mami.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select u from User u where u.token = :token")
    User findByByToken(String token);

}
