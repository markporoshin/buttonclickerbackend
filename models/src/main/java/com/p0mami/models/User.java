package com.p0mami.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "token")
    private String token;

    @Column(name = "counter")
    private Long counter;

    public User() {
    }

    public User(Long id, String token, Long counter) {
        this.id = id;
        this.token = token;
        this.counter = counter;
    }

    @Override
    public String toString() {
        return "User{id=" + id + ", counter=" + counter + ", token=" + token + "}";
    }
}
