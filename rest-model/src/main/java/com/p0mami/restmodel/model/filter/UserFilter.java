package com.p0mami.restmodel.model.filter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserFilter {

    private String token;

    public UserFilter(String token) {
        this.token = token;
    }

    public UserFilter() {

    }


}
