package com.p0mami.restmodel.model.post;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorInfo {
    private String message;
    private String url;
    private String error;
    private String path;

    public ErrorInfo(String message, String url, String path, String error) {
        this.message = message;
        this.url = url;
        this.error = error;
    }
}
