package com.p0mami.restmodel.model.post;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserPost {

    private Long counter;

    public UserPost() {

    }

    public UserPost(Long counter) {
        this.counter = counter;
    }
}
