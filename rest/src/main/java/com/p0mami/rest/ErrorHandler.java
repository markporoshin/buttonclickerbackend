package com.p0mami.rest;

import com.p0mami.restmodel.model.exceprion.ObjectNotFound;
import com.p0mami.restmodel.model.post.ErrorInfo;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(value =  ObjectNotFound.class)
    @ResponseBody
    public ErrorInfo handleException(HttpServletRequest req, ObjectNotFound e) {
        return new ErrorInfo(e.getMessage(), req.getRequestURL().toString(), req.getServletPath(), e.getClass().getSimpleName());
    }
}
