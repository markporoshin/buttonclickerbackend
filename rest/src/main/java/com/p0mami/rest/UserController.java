package com.p0mami.rest;

import com.p0mami.core.user.UserService;
import com.p0mami.domain.user.UserDomainService;
import com.p0mami.models.User;
import com.p0mami.restmodel.model.exceprion.ObjectNotFound;
import com.p0mami.restmodel.model.filter.UserFilter;
import com.p0mami.restmodel.model.post.UserPost;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    @PostMapping("/user")
    @ResponseBody
    public UserPost findUserByToken(@RequestBody UserFilter filter) {
        return userService.findOrCreate(filter);
    }

    @PostMapping("/user/increment")
    public UserPost incrementByToken(@RequestBody UserFilter filter) {
        return userService.increment(filter);
    }

}
